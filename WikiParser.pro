# -------------------------------------------------
# Project created by QtCreator 2011-10-30T13:53:32
# -------------------------------------------------
QT += core \
    network \
    xml \
    sql
QT -= gui
TARGET = WikiParser
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += main.cpp \
    wiktionaryparser.cpp \
    wiktionaryentry.cpp \
    wiktionaryparserutilities.cpp
HEADERS += wiktionaryparser.h \
    wiktionaryentry.h \
    wiktionaryparserutilities.h
