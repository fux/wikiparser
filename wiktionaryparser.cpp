/*  This file is part of the WikiParser
    Copyright (C) 2011-2012 Mario Fux <fux@kde.org>

    WikiParser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WikiParser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WikiParser.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QCoreApplication>
#include <QtCore/QChar>
#include <QtCore/QDebug>
#include <QtCore/QEventLoop>
#include <QtCore/QFile>
#include <QtCore/QTime>
#include <QtNetwork/QNetworkProxy>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>
#include <QtXml/QDomDocument>
#include <QtXml/QDomNamedNodeMap>
#include <QtXml/QDomNodeList>

#include "wiktionaryparser.h"
#include "wiktionaryparserutilities.h"

///////////////////////////////////////////////////////////////////////////////
// Constructors and destructors
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Constructors
WiktionaryParser::WiktionaryParser(QObject *parent)
    : QObject(parent)
{
    qDebug() << "WiktionaryParser construction ...";
    m_wordCount = 0;
    m_doubleCount = 0;

    ///////////////////////////
    // Variable initialization
    m_networkManager = new QNetworkAccessManager(this);
    m_language = "de";
    m_baseUrl = "http://" + m_language + ".wiktionary.org";
    m_wikiUrl = m_baseUrl + "/wiki/";
    qDebug() << "Wiktionary wikiUrl:" << m_wikiUrl;

    ///////////////////////////
    // Database initialization
    m_db = QSqlDatabase::addDatabase("QMYSQL");
    m_db.setUserName("wpuser");
    m_db.setPassword("wp123");
    m_db.setDatabaseName("wiktionaryParserDb");
    if (m_db.open()) {
        qDebug() << "Db connection succeeded...";
    }
    else {
        qDebug() << "Problem with db connection!";
    }

    // deWiktionary TABLE creation query
    QSqlQuery query("CREATE TABLE deWiktionary (word VARCHAR(100) PRIMARY KEY, wordForm VARCHAR(100), wordUrl VARCHAR(200), partOfSpeechTag VARCHAR(20), hyphenations VARCHAR(500), pronunciations VARCHAR(500), meanings TEXT, etymologies TEXT, synonyms TEXT, antonyms TEXT, hyperonyms TEXT, hyponyms TEXT, examples TEXT) ENGINE = InnoDB CHARACTER SET utf8 COLLATE = utf8_general_ci");
    // TODO: noun TABLE creation query
    QSqlQuery nounQuery("CREATE TABLE noun (word VARCHAR(50) PRIMARY KEY, AccSg VARCHAR(100), AccPl VARCHAR(100), DatSg VARCHAR(100), DatPl VARCHAR(100), GenSg VARCHAR(100), GenPl VARCHAR(100), NomSg VARCHAR(100), NomPl VARCHAR(100)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE = utf8_general_ci");
    // TODO: verb TABLE creation query
    QSqlQuery verbQuery("CREATE TABLE verb (word VARCHAR(50) PRIMARY KEY, 1stPersSgPresent VARCHAR(100), 2ndPersSgPresent VARCHAR(100), 3rdPersSgPresent VARCHAR(100), 1stPersSgPast VARCHAR(100), ParticipleII VARCHAR(100), 1stPersSgConjunctiveII VARCHAR(100), SgImperative VARCHAR(100), PlImperative VARCHAR(100), Auxiliary VARCHAR(100)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE = utf8_general_ci");
    // TODO: adjective TABLE creation query
    QSqlQuery adjectiveQuery("CREATE TABLE adjective (word VARCHAR(50) PRIMARY KEY, Positive VARCHAR(100), Comparative VARCHAR(100), Superlative VARCHAR(100)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE = utf8_general_ci");
    // TODO: fullform TABLE creation query
    QSqlQuery fullformQuery("CREATE TABLE fullform (wordForm VARCHAR(50) PRIMARY KEY, word VARCHAR(50)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE = utf8_general_ci");
    // translations TABLE creation query
    QSqlQuery translationsQuery("CREATE TABLE translations (word VARCHAR(50) PRIMARY KEY, translations TEXT) ENGINE = InnoDB CHARACTER SET utf8 COLLATE = utf8_general_ci");

    ////////////////////////////////
    // Setup posMap
    // Link: http://de.wiktionary.org/wiki/Kategorie:Deutsch
//    m_posMap.insert("Adjektiv", "adjective");
//    m_posMap.insert("Adverb", "adverb");
    m_posMap.insert("Interrogativadverb", "adverb");
    m_posMap.insert("Konjunktionaladverb", "adverb");
    m_posMap.insert("Lokaladverb", "adverb");
    m_posMap.insert("Modaladverb", "adverb");
    m_posMap.insert("Pronominaladverb", "adverb");
    m_posMap.insert("Temporaladverb", "adverb");
//    m_posMap.insert("Antwortpartikel", "particle");
//    m_posMap.insert("Artikel", "article");
//    m_posMap.insert("Eigenname", "noun");
//    m_posMap.insert("Fokuspartikel", "particle");
//    m_posMap.insert("Gradpartikel", "particle");
//    m_posMap.insert("Hilfsverb", "verb");
//    m_posMap.insert("Interjektion", "interjection");
//    m_posMap.insert("Konjunktion", "conjunction");
//    m_posMap.insert("Kontraktion", "contraction");
//    m_posMap.insert("Modalpartikel", "particle");
//    m_posMap.insert("Negationspartikel", "particle");
//    m_posMap.insert("Numerale", "numeral");
//    m_posMap.insert("Partikel", "particle");
//    m_posMap.insert("Partizip_I", "participle");
//    m_posMap.insert("Partizip_II", "participle");
//    m_posMap.insert("Pluraletantum", "noun");
//    m_posMap.insert("Postposition", "postposition");
//    m_posMap.insert("Pronomen", "pronoun");
//    m_posMap.insert("Demonstrativpronomen", "pronoun");
//    m_posMap.insert("Indefinitpronomen", "pronoun");
//    m_posMap.insert("Interrogativpronomen", "pronoun");
//    m_posMap.insert("Personalpronomen", "pronoun");
//    m_posMap.insert("Possessivpronomen", "pronoun");
//    m_posMap.insert("Reflexivpronomen", "pronoun");
//    m_posMap.insert("Relativpronomen", "pronoun");
//    m_posMap.insert("Reziprokpronomen", "pronoun");
//    m_posMap.insert(QString::fromUtf8("Präposition"), "preposition");
//    m_posMap.insert("Singularetantum", "noun");
//    m_posMap.insert("Subjunktion", "subjunction");
//    m_posMap.insert("Substantiv", "noun");
//    m_posMap.insert("Verb", "verb");
//    m_posMap.insert("Vergleichspartikel", "particle");
//    // TODO: Really???
//    // m_posMap.insert("Wortverbindung", "");
//    // TODO: "zig" is not a noun...
//    m_posMap.insert("Zahlklassifikator", "noun");
    // TODO: Interesting category but not a unique pos tag possible
    // m_posMap.insert("Fremdwort", "");
    // More:
//    m_posMap.insert("Konjugierte_Form", "conjugation");
//    m_posMap.insert("Deklinierte_Form", "declination");
}

///////////////////////////////////////////////////////////////////////////////
// Destructors
WiktionaryParser::~WiktionaryParser()
{
    qDebug() << "WiktionaryParser destruction ...";
}




///////////////////////////////////////////////////////////////////////////////
// Public Methods
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Sets the m_wikiUrl variable
void WiktionaryParser::setWikiUrl(QString wikiUrl) {
    m_wikiUrl = wikiUrl;
}

///////////////////////////////////////////////////////////////////////////////
// Downloads the list of words of a given part of speech tag
QStringList WiktionaryParser::downLoadWordListByPos(QString l10nPosTag)
{
    QStringList wordList;
    WiktionaryParserUtilities wpu;

    qDebug() << "Extracting" << l10nPosTag << "/" << m_posMap.value(l10nPosTag);
    wordList = getWordList(l10nPosTag);
    qDebug() << "Number of" << l10nPosTag << "found:" << wordList.length();
    // TODO: Check if there is already a wordList, add to it then and read to m_wordLists
    // m_wordLists.insert(m_posMap.value(l10nPosTag), wordList);

//    if (posTag == "verb") {
//        // German verbs http://de.wiktionary.org/wiki/Kategorie:Verb_(Deutsch)
//        qDebug() << "Extracting" << posTag << "list ...";
//        wordList = getWordList("Verb");
//        qDebug() << "Number of" << posTag << "found:" << wordList.length();
//        m_wordLists.insert(posTag, wordList);
//    }
//    else if (posTag == "noun") {
//        // German nouns http://de.wiktionary.org/wiki/Kategorie:Substantiv_(Deutsch)
//        qDebug() << "Extracting" << posTag << "list ...";
//        wordList = getWordList("Substantiv");
//        qDebug() << "Number of" << posTag << "found:" << wordList.length();
//        m_wordLists.insert(posTag, wordList);
//    }
//    else if (posTag == "adjective") {
//        // German adjectives http://de.wiktionary.org/wiki/Kategorie:Adjektiv_(Deutsch)
//        qDebug() << "Extracting" << posTag << "list ...";
//        wordList = getWordList("Adjektiv");
//        qDebug() << "Number of" << posTag << "found:" << wordList.length();
//        m_wordLists.insert(posTag, wordList);
//    }
//    else if (posTag == "adverb") {
//        // German adjectives http://de.wiktionary.org/wiki/Kategorie:Adverb_(Deutsch)
//        qDebug() << "Extracting" << posTag << "list ...";
//        wordList = getWordList("Adverb");
//        qDebug() << "Number of" << posTag << "found:" << wordList.length();
//        m_wordLists.insert(posTag, wordList);
//    }
//    else if (posTag == "singularetantum") {
//        // German adjectives http://de.wiktionary.org/wiki/Kategorie:Singularetantum_(Deutsch)
//        qDebug() << "Extracting" << posTag << "list ...";
//        wordList = getWordList("Singularetantum");
//        qDebug() << "Number of" << posTag << "found:" << wordList.length();
//        m_wordLists.insert(posTag, wordList);
//    }
//    else if (posTag == "subjunktion") {
//        // German adjectives http://de.wiktionary.org/wiki/Kategorie:Subjunktion_(Deutsch)
//        qDebug() << "Extracting" << posTag << "list ...";
//        wordList = getWordList("Subjunktion");
//        qDebug() << "Number of" << posTag << "found:" << wordList.length();
//        m_wordLists.insert(posTag, wordList);
//    }
//    else if (posTag == "fremdwort") {
//        // German adjectives http://de.wiktionary.org/wiki/Kategorie:Fremdwort_(Deutsch)
//        qDebug() << "Extracting" << posTag << "list ...";
//        wordList = getWordList("Fremdwort");
//        qDebug() << "Number of" << posTag << "found:" << wordList.length();
//        m_wordLists.insert(posTag, wordList);
//    }
//    else if (posTag == "all") {
//        qDebug() << "Extracting" << posTag << "list ...";
//        wordList = getWordList("all");
//        wpu.saveWordList(wordList, "/home/fmario/deWiktionary/all.list");
//        qDebug() << "Number of" << posTag << "found:" << wordList.length();
//        m_wordLists.insert(posTag, wordList);
//    }
//    else {
//        qDebug() << "Error in extracting" << posTag << "list!";
//    }

    return wordList;
}

///////////////////////////////////////////////////////////////////////////////
//
void WiktionaryParser::downLoadWiktionaryPages(QStringList wordList, QString baseSaveDir, QString posTag)
{
    WiktionaryParserUtilities wpu;

    for (int i = 0; i < wordList.length(); i++) {
        QString word = wordList.at(i);
        if (word.contains(" ")) {
            word.replace(" ", "_");
        }
        qDebug() << "Download word:" << word;
        QString fileName = baseSaveDir + word + ".xhtml";
        QFile saveFile(fileName);
        if (!saveFile.open(QIODevice::WriteOnly)) {
            qDebug() << "Error while opening file" << fileName;
        }
        QString wordUrl = m_wikiUrl + word;
        saveFile.write(downloadPage(wordUrl));
        saveFile.close();
    }

    wpu.saveWordList(wordList, baseSaveDir + posTag + ".list");
}

///////////////////////////////////////////////////////////////////////////////
// Parses a wiktionary page and extracts all the information
QList<WiktionaryEntry> WiktionaryParser::parseWiktionaryPage(QString word, bool remote)
{
    QList<WiktionaryEntry> wEntryList;
    QString wordForUrl = word;

    if (word.contains("_")) {
        qDebug() << "Error: Word contains underscore (_)...";
        return wEntryList;
    }

    if (wordForUrl.contains(" ")) {
        wordForUrl.replace(" ", "_");
    }

    QByteArray wordPageContent;
    QString baseUrl;
    // TODO: Make it more senseful (two methods, one for remote and one for local)
    if (remote) {
        baseUrl = m_wikiUrl + wordForUrl;
        wordPageContent = downloadPage(baseUrl);
    }
    else {
        QFile wikiPageFile(wordForUrl.prepend(m_wikiUrl).append(".xhtml"));
        if (!wikiPageFile.open(QIODevice::ReadOnly)) {
            qDebug() << "Error while opening file" << wordForUrl;
        }
        wordPageContent = wikiPageFile.readAll();
        baseUrl = wordForUrl;
    }

    QDomDocument doc;
    doc.setContent(wordPageContent);

    QDomNodeList header2List = doc.elementsByTagName("h2");

    // Run trough all the language entries (e.g. "Wort (Dänisch)")
    for (unsigned int i = 0; i < header2List.length(); i++) {

        QString localeStr = word + " (Deutsch)";

        QString ts = header2List.at(i).toElement().text();

        qDebug() << header2List.at(i).toElement().text() << localeStr << ts;
        // Parse just the German entries (e.g. "Word (Deutsch)")

        if (header2List.at(i).toElement().text().endsWith(localeStr)) {

            int wordNumber = 1;
            QString str;

            // get part of speech tag
            QDomNode tempNode;
            tempNode = header2List.at(i);

            // TODO: Change WiktionaryEntry as we need another word as indentifier (index in db)
            // TODO: Add a posTag translation table (e.g. "Nachname" -> "noun")
            WiktionaryEntry wEntry(word + "_" + str.setNum(wordNumber));
            wEntry.setWordUrl(baseUrl);
            wEntry.setWordForm(word);

            bool firstEntry = true;

            while(!tempNode.nextSiblingElement().nodeName().contains("h2") && tempNode.nextSiblingElement().isElement()) {
                tempNode = tempNode.nextSibling();


                if (tempNode.nodeName().contains("h3")) {
                    QString posTag;
                    if (firstEntry) {
                        m_wordCount++;
                        m_doubleCount++;
                        firstEntry = false;
                        posTag = parsePosTag(tempNode.toElement().text());
                        wEntry.setPartOfSpeechTag(posTag);
                        // If next element is a table element then parse it for inflection data
                        if (tempNode.nextSibling().nodeName().contains("table")) {
                            tempNode = tempNode.nextSibling();
                            wEntry.setInflectionData(parseInflectionTable(posTag, tempNode));
                        }
                    }
                    else {
                        wEntry.showContent();
                        wEntryList.append(wEntry);
                        wordNumber++;
                        m_doubleCount++;
                        wEntry.clear();
                        str.clear();
                        wEntry.setWord(word + "_" + str.setNum(wordNumber));
                        wEntry.setWordUrl(baseUrl);
                        posTag = parsePosTag(tempNode.toElement().text());
                        wEntry.setPartOfSpeechTag(posTag);
                        // If next element is a table element then parse it for inflection data
                        if (tempNode.nextSibling().nodeName().contains("table")) {
                            tempNode = tempNode.nextSibling();
                            wEntry.setInflectionData(parseInflectionTable(posTag, tempNode));
                        }
                    }
                    wEntry.setWordForm(word);
                }

                // Get hyphenations
                // TODO: Better parser (more lines (see below))
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Silbentrennung:")) {
                    wEntry.setHyphenations(tempNode.nextSiblingElement().text().split(","));
                }
                // Get pronunciation
                // TODO: Better parser (with "Partizip and Co and more lines "Horbeispiele")
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Aussprache:")) {
                    wEntry.setPronunciations(parsePronunciation(tempNode.nextSiblingElement().text()));
                }
                // Get meanings
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Bedeutungen:")) {
                    wEntry.setMeanings(parseDdTags(tempNode.nextSibling().childNodes()));
//                    qDebug() << "M:" << parseDdTags(tempNode.nextSibling().childNodes());
                }
                // Get etymologies
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Herkunft:")) {
                    wEntry.setEtymologies(parseDdTags(tempNode.nextSibling().childNodes()));
                }
                // Get synonyms
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Synonyme:")) {
                    wEntry.setSynonyms(parseDdTags(tempNode.nextSibling().childNodes()));
                }
                // Get antonyms
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Gegenw")) {
                    wEntry.setAntonyms(parseDdTags(tempNode.nextSibling().childNodes()));
                }
                // Get hyperonyms
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Oberbegriffe")) {
                    wEntry.setHyperonyms(parseDdTags(tempNode.nextSibling().childNodes()));
                }
                // Get hyponyms
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Unterbegriffe")) {
                    wEntry.setHyponyms(parseDdTags(tempNode.nextSibling().childNodes()));
                }
                // Get examples
                if ((tempNode.nodeName() == "p") && tempNode.toElement().text().startsWith("Beispiele:")) {
                    wEntry.setExamples(parseDdTags(tempNode.nextSibling().childNodes()));
                }
                // Get translations
                if ((tempNode.nodeName() == "h4") && tempNode.toElement().text().contains(QString::fromUtf8("Übersetzungen"))) {
                    // FIXME: Make this table element detection more robust!!!
                    QStringList tempTranslationsList;
                    QDomNode translationTableNode = tempNode.nextSibling().childNodes().at(0).childNodes().at(1).childNodes().at(0);
                    qDebug() << "Translations:";
                    if (translationTableNode.nodeName() == "table") {
                        if (translationTableNode.childNodes().at(0).nodeName() == "tr") {
                            QDomNodeList ttableColumns = translationTableNode.childNodes().at(0).childNodes();
                            for (unsigned int k = 0; k < ttableColumns.length(); k++) {
                                if (ttableColumns.at(k).childNodes().at(0).nodeName() == "ul") {
                                    QDomNodeList translations = ttableColumns.at(k).childNodes().at(0).childNodes();
                                    for (unsigned int l = 0; l < translations.length(); l++) {
//                                        qDebug() << k << l << translations.at(l).toElement().text();
                                        tempTranslationsList.append(getChildNodeStrings(translations.at(l), "sup"));
                                    }
                                }
                            }
                        }
                    }
                    wEntry.setTranslations(tempTranslationsList);
                }
            }
            wEntry.showContent();
            wEntryList.append(wEntry);
        }
    }

    return wEntryList;
}


///////////////////////////////////////////////////////////////////////////////
// Parses a list of works and extracts their information
void WiktionaryParser::parseAllWords(QString posTag)
{
    QStringList wordList = m_wordLists.value(posTag);
    WiktionaryParserUtilities wpu;

    QSqlQuery query;

    int someCount = 0;
    QStringList someList;

    // FIXME int to long ????
    for (int i = 0; i < wordList.length(); i++) {
        qDebug() << "Parse word:" << wordList.at(i);
        QList<WiktionaryEntry> wEntryList = parseWiktionaryPage(wordList.at(i));

        if (wEntryList.length() == 0) {
            someCount++;
            someList.append(wordList.at(i));
        }

        QStringList fullformQueries;
        for (int j = 0; j < wEntryList.length(); j++) {
            query.prepare(createPosTagInsertQuery(wEntryList.at(j), wEntryList.at(j).partOfSpeechTag()));
            query.exec();
            query.clear();
            query.prepare(createInsertQuery(wEntryList.at(j), "deWiktionary"));
            query.exec();
            query.clear();
            fullformQueries = createFullformInsertQueries(wEntryList.at(j), wEntryList.at(j).partOfSpeechTag());
            for (int k = 0; k < fullformQueries.length(); k++) {
                query.prepare(fullformQueries.at(k));
                query.exec();
                query.clear();
            }

            query.prepare(createTranslationInsertQuery(wEntryList.at(j), "translations"));
            query.exec();
            query.clear();

//            qDebug() << "SQL query:" << createPosTagInsertQuery(wEntryList.at(j), wEntryList.at(j).partOfSpeechTag());
//            qDebug() << "SQL:" << createInsertQuery(wEntryList.at(j), "deWiktionary");
//            qDebug() << "SQL ff:" << fullformQueries;
        }
    }

    wpu.saveWordList(someList, "/home/fmario/false" + posTag + ".list");
    qDebug() << "Count:" << m_wordCount << m_doubleCount << someCount;
}

///////////////////////////////////////////////////////////////////////////////
// Gets a list of words from a local file
QStringList WiktionaryParser::getWordListFromFile(QString path, QString posTag)
{
    QStringList wordList;
    QString filename = path.append(posTag).append(".list");
    QFile savefile(filename);

    if (!savefile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error while reading from file" << filename;
    }

    while (!savefile.atEnd()) {
        QByteArray wordArray = savefile.readLine().replace("\n", "");
        QString word = QString::fromUtf8(wordArray);
        wordList.append(word);
//        qDebug() << wordArray << word;
    }
    m_wordLists.insert(posTag, wordList);

    return wordList;
}

///////////////////////////////////////////////////////////////////////////////
// Analyse a list of words and check if there are in the db
bool WiktionaryParser::analyseText(QStringList tokenizedText)
{
    QStringList missingWords;

    QRegExp rx("(\\d+)");
    QSqlQuery searchQuery;
    double counter = 1;
    double found = 0;
    QString word, queryResult, posTag;
    QStringList::const_iterator constIterator;
    for (constIterator = tokenizedText.constBegin(); constIterator != tokenizedText.constEnd(); ++constIterator) {
        word = (*constIterator);

        // Check if it's a number
        if (word.contains(rx)) {
            found++;
            qDebug() << counter << word << "number";
            counter++;
            continue;
        }

        // FIXME: Remove this and make word "sein" parsable
        if (word.startsWith("ist") || word.startsWith("sein") || word.startsWith("sind") || word.startsWith("sei")) {
            found++;
            qDebug() << counter << word << "verb \"sein\"";
            counter++;
            continue;
        }

        searchQuery.prepare("SELECT * FROM `fullform` WHERE `wordform` LIKE '" + word + "'");
        searchQuery.exec();
        if (searchQuery.next()) {
          queryResult = searchQuery.value(1).toString();
//          posTag = searchQuery.value(3).toString();
          found++;
        }
        else {
            missingWords.append(word);
        }
        qDebug() << counter << word << queryResult << posTag;
        counter++;
        searchQuery.clear();
        queryResult.clear();
        posTag.clear();
    }
    qDebug() << "Found" << found << "words of" << counter << "which is" << found/counter*100 << "%";
    qDebug() << "Missing words:" << missingWords;

    return true;
}


QMap<QString, QString> WiktionaryParser::getL10nPosTagMap()
{
    return m_posMap;
}


///////////////////////////////////////////////////////////////////////////////
// Private methods
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Downloads a (x)html page via HTTP and returns the content
QByteArray WiktionaryParser::downloadPage(QString urlString)
{
    // TODO: Idea from http://www.developer.nokia.com/Community/Wiki/How_to_wait_synchronously_for_a_Signal_in_Qt

    QUrl url = QUrl::fromUserInput(urlString);

    QNetworkReply *reply = m_networkManager->get(QNetworkRequest(url));

    QEventLoop loop;
    QObject::connect(m_networkManager, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));

    // Execute the event loop here, now we will wait here until readyRead() signal is emitted
    // which in turn will trigger event loop quit.
    loop.exec();

    QByteArray result = reply->readAll();

//    qDebug() << "Url:" << url;
//    qDebug() << "Download length:" << result.length();

    return result;
}

///////////////////////////////////////////////////////////////////////////////
//
QStringList WiktionaryParser::getWordList(QString l10nPos)
{
    QStringList wordList;

    QString posUrl = "Kategorie:" + l10nPos + "_(Deutsch)";

    // FIXME: Remove this hack
    if (l10nPos.contains("Fremdwort")) {
        posUrl.replace("_(Deutsch)", "");
    }

    QString baseUrl = m_wikiUrl + posUrl;

    if (l10nPos == "all") {
        baseUrl = m_wikiUrl + "Kategorie:Deutsch";
    }

    int numberOfPages = getNumberOfPages(baseUrl);

    wordList = parseWordListPage(downloadPage(baseUrl));
    QStringList tempList;
    for (int i = 0; i < numberOfPages; i++) {
        tempList.clear();
        baseUrl = getNextUrl(baseUrl);
        tempList = parseWordListPage(downloadPage(baseUrl));
        wordList.append(tempList);
    }

    return wordList;
}

///////////////////////////////////////////////////////////////////////////////
//
int WiktionaryParser::getNumberOfPages(QString urlString)
{
    int wordsPerPage = 200;
    int numberOfPages, numberOfWords;
    QString numberOfWordsString;

    QByteArray pageContent = downloadPage(urlString);

    QDomDocument doc;
    doc.setContent(pageContent);

    QDomNodeList paragraphNodes = doc.elementsByTagName("p");

    for (unsigned int i = 0; i < paragraphNodes.length(); i++) {
//        qDebug() << i << paragraphNodes.at(i).toElement().text();
        QString searchString = QString::number(wordsPerPage) + " von insgesamt";
        if (paragraphNodes.at(i).toElement().text().contains(searchString)) {
//            qDebug() << "Juhu" << paragraphNodes.at(i).toElement().text();
            // TODO: Make this number of pages parsing more generic and an own method
            numberOfWordsString = paragraphNodes.at(i).toElement().text().split(" ").at(5);
            break;
        }
    }

    QString tempStr = numberOfWordsString.remove(".");
    numberOfWords = tempStr.toInt();

    numberOfPages = numberOfWords / wordsPerPage + 1;

    return numberOfPages;
}

///////////////////////////////////////////////////////////////////////////////
//
QString WiktionaryParser::getNextUrl(QString urlString)
{
    int wordsPerPage = 200;
    QString nextUrl;

    QByteArray pageContent = downloadPage(urlString);

    QDomDocument doc;
    doc.setContent(pageContent);

    QDomNodeList linkNodes = doc.elementsByTagName("a");

    QDomElement nextLinkElement;

    for (unsigned int i = 0; i < linkNodes.length(); i++) {
        QString searchString = "chste " + QString::number(wordsPerPage);
        if (linkNodes.at(i).toElement().text().contains(searchString)) {
            nextLinkElement = linkNodes.at(i).toElement();
            break;
        }
    }

    nextUrl = m_baseUrl + nextLinkElement.attribute("href");

    return nextUrl;
}

///////////////////////////////////////////////////////////////////////////////
//
QStringList WiktionaryParser::parseWordListPage(QByteArray pageContent)
{
    QStringList wordList;

    QDomDocument doc;
    doc.setContent(pageContent);

    QDomNodeList header3List = doc.elementsByTagName("h3");

    // Extract the words of the page
    for (unsigned int i = 0; i < header3List.length(); i++) {
//        qDebug() << i << tempList.at(i).toElement().text();
        QDomNode node = header3List.at(i).nextSibling();
        QDomNodeList liList = node.childNodes();
        for (unsigned int j = 0; j < liList.length(); j++) {
//            qDebug() << i << j << liList.at(j).toElement().text();
            if (liList.at(j).childNodes().at(0).toElement().attribute("href").startsWith("/wiki/")) {
                wordList.append(liList.at(j).toElement().text());
//                qDebug() << i << j << liList.at(j).toElement().text();
            }
        }
    }
    qDebug() << "Number of extracted words:" << wordList.length();

    return wordList;
}

///////////////////////////////////////////////////////////////////////////////
//
QStringList WiktionaryParser::getRandomWordList(QStringList wordList, int numberOfWords)
{
    QStringList resultList;

    qsrand(QTime::currentTime().msec());

    for (int i = 0; i < numberOfWords; i++) {
        resultList.append(wordList.at(qrand()%(wordList.length())));
        if (resultList.removeDuplicates() > 0) {
            i--;
        }
    }

    return resultList;
}

///////////////////////////////////////////////////////////////////////////////
//
QStringList WiktionaryParser::parsePronunciation(QString pronunciationLines)
{
    // TODO: Add semantics to the IPAs
    QStringList tempList, pronunciationList;

//    qDebug() << "Pronuncation lines:" << pronunciationLines;

    tempList = pronunciationLines.split("[");

    QString tempStr;
    foreach (tempStr, tempList) {
        if (tempStr.contains("]") && !(tempStr.contains(QString::fromUtf8("…")) || tempStr.contains("..."))) {
            pronunciationList.append(tempStr.split("]").at(0));
        }
    }

    return pronunciationList;
}

///////////////////////////////////////////////////////////////////////////////
// Parse a list of information with bracketed numbers
// DEPRECATED
QStringList WiktionaryParser::parseNumberedList(QString numberedListLines)
{
    QStringList tempList = numberedListLines.split("[");

    QStringList resultList;
    for (int i = 0; i < tempList.length(); i++) {
        QString tempStr = tempList.at(i);
        if (!tempStr.isEmpty()) {
            if (i != 0) {
                tempStr.prepend("[");
            }
            resultList.append(tempStr);
        }
    }

    tempList.clear();
    for (int i = 0; i < resultList.length(); i++) {
        QString tempStr = resultList.at(i);
        if (tempStr.startsWith("[") && tempStr.endsWith("]"))
        {
            continue;
        }
        tempList.append(tempStr);
    }

    return tempList;
}

///////////////////////////////////////////////////////////////////////////////
// Parses the part of speech tag
QString WiktionaryParser::parsePosTag(QString posTagLine)
{
    // TODO: Simplify this thing

    WiktionaryParserUtilities wpu;
    QString posTag;

    if (posTagLine.contains(",")) {
        QString tempStr = posTagLine.split(",").at(0);
        posTag = wpu.getL10nPosTag(tempStr);
        if (tempStr.contains("]")) {
            posTag = wpu.getL10nPosTag(tempStr.split("]").at(1));
        }
    }
    else {
        posTag = wpu.getL10nPosTag(posTagLine);
        if (posTagLine.contains("]")) {
            posTag = wpu.getL10nPosTag(posTagLine.split("]").at(1));
        }
    }

    return posTag;
}

///////////////////////////////////////////////////////////////////////////////
// Parses the inflection table
QMap<QString, QString> WiktionaryParser::parseInflectionTable(QString posTag, QDomNode tableNode)
{
    QMap<QString, QString> inflectionData;

    if (posTag.startsWith("verb")) {
        inflectionData = parseConjugationTable(tableNode);
    }
    else if (posTag.startsWith("noun")) {
        inflectionData = parseDeclensionTable(tableNode);
    }
    else if (posTag.startsWith("adjective")) {
        inflectionData = parseComparisonTable(tableNode);
    }
    else {
        qDebug() << "PosTag not known:" << posTag;
    }

//    Debug output to determine the right table cells
//    qDebug() << "Inflection table:";
//    QDomNodeList tableRowList = tableNode.childNodes();
//    QDomNodeList rowDataList;
//    // Start at 1 (one) as we don't need the first line (header line)
//    for (unsigned int i = 1; i < tableRowList.length(); i++) {
//        qDebug() << tableRowList.at(i).toElement().text();
//        rowDataList = tableRowList.at(i).childNodes();
//        // j starts with 1 as we dont need the header row
//        for (unsigned int j = 0; j < rowDataList.length(); j++) {
//            qDebug() << i << j << rowDataList.at(j).toElement().text();
//            if (rowDataList.at(j).toElement().text().contains("\n")) {
//                qDebug() << "Contains newlines => more than one entry";
//            }
//        }
//    }
//    qDebug();

    return inflectionData;
}

///////////////////////////////////////////////////////////////////////////////
// Parses the declension table of nouns
QMap<QString, QString> WiktionaryParser::parseDeclensionTable(QDomNode tableNode)
{
    QMap<QString, QString> declensionData;

    QDomNodeList tableRowList = tableNode.childNodes();

    // Check if there are pictures above the table and when the real content begins
    unsigned int i = 0;
    for (; i < tableRowList.length(); i++) {
        if (tableRowList.at(i).toElement().text().startsWith("Kasus")) {
            break;
        }
    }

    // TODO: Check if we're really in the correct line
    declensionData.insert("NomSg", getCellData(tableNode, 1+i, 1));
    declensionData.insert("NomPl", getCellData(tableNode, 1+i, 2));
    declensionData.insert("GenSg", getCellData(tableNode, 2+i, 1));
    declensionData.insert("GenPl", getCellData(tableNode, 2+i, 2));
    declensionData.insert("DatSg", getCellData(tableNode, 3+i, 1));
    declensionData.insert("DatPl", getCellData(tableNode, 3+i, 2));
    declensionData.insert("AccSg", getCellData(tableNode, 4+i, 1));
    declensionData.insert("AccPl", getCellData(tableNode, 4+i, 2));

    return declensionData;
}

///////////////////////////////////////////////////////////////////////////////
// Parses the conjugation table of verbs
QMap<QString, QString> WiktionaryParser::parseConjugationTable(QDomNode tableNode)
{
    QMap<QString, QString> conjugationData;

    conjugationData.insert("1stPersSgPresent", getCellData(tableNode, 1, 2));
    conjugationData.insert("2ndPersSgPresent", getCellData(tableNode, 2, 1));
    conjugationData.insert("3rdPersSgPresent", getCellData(tableNode, 3, 1));
    conjugationData.insert("1stPersSgPast", getCellData(tableNode, 4, 2));
    conjugationData.insert("ParticipleII", getCellData(tableNode, 5, 2));
    conjugationData.insert("1stPersSgConjunctiveII", getCellData(tableNode, 6, 2));
    conjugationData.insert("SgImperative", getCellData(tableNode, 7, 2));
    conjugationData.insert("PlImperative", getCellData(tableNode, 8, 1));
    conjugationData.insert("Auxiliary", getCellData(tableNode, 9, 2));

//    qDebug() << "ConjugationData:" << conjugationData;

    return conjugationData;
}

///////////////////////////////////////////////////////////////////////////////
// Parses the comparison table of adjectives
QMap<QString, QString> WiktionaryParser::parseComparisonTable(QDomNode tableNode)
{
    QMap<QString, QString> comparisonData;

    comparisonData.insert("Positive", getCellData(tableNode, 1, 0));
    comparisonData.insert("Comparative", getCellData(tableNode, 1, 1));
    comparisonData.insert("Superlative", getCellData(tableNode, 1, 2));

//    qDebug() << "ComparisonData:" << comparisonData;

    return comparisonData;
}

///////////////////////////////////////////////////////////////////////////////
// Gets data from a certain cell of the table
QString WiktionaryParser::getCellData(QDomNode tableNode, unsigned int row, unsigned int column)
{
    QString result;

    if (tableNode.childNodes().length() > row) {
        QDomNode rowNode = tableNode.childNodes().at(row);
        if (rowNode.childNodes().length() > column) {
            result = rowNode.childNodes().at(column).toElement().text();
        }
    }

    return result;
}

///////////////////////////////////////////////////////////////////////////////
// Converts the dd html tags to a string list
QStringList WiktionaryParser::parseDdTags(QDomNodeList ddList)
{
    QStringList resultList;

    for (unsigned int i = 0; i < ddList.length(); i++) {
        resultList.append(ddList.at(i).toElement().text());
    }

    return resultList;
}

///////////////////////////////////////////////////////////////////////////////
// Check childNode attributes if they contain a certain string
bool WiktionaryParser::checkChildNodeAttributes(QDomNodeList childNodes, QString string)
{
    // My first recursive method ;-)
    bool found = false;

    for (unsigned int i = 0; i < childNodes.length(); i++) {
//        qDebug() << "In first for loop";
        if (childNodes.at(i).hasAttributes()) {
//            qDebug() << "Attributes found in" << childNodes.at(i).nodeName();
            QDomNamedNodeMap attributeList = childNodes.at(i).attributes();
            for (unsigned int k = 0; k < attributeList.length(); k++) {
                if (attributeList.item(k).nodeValue().contains(string)) {
//                    qDebug() << "Attribute:" << attributeList.item(k).nodeName() << attributeList.item(k).nodeValue();
                    found = true;
                    // If we found something we don't need to search further
                    break;
                }
            }
        }

        // We just need to check more childnodes if we didn't not yet find something
        if (!found && childNodes.at(i).hasChildNodes()) {
//            qDebug() << "Recursion to childNodes";
            found = checkChildNodeAttributes(childNodes.at(i).childNodes(), string);
        }
    }

    return found;
}

///////////////////////////////////////////////////////////////////////////////
// Return strings of all (or certain) childdomnodes
QString WiktionaryParser::getChildNodeStrings(QDomNode domNode, QString excludeNode)
{
    QString returnStr;

    if (domNode.hasChildNodes()) {
        QString tempStr;
        for (unsigned int i = 0; i < domNode.childNodes().length(); i++) {
            tempStr = getChildNodeStrings(domNode.childNodes().at(i), excludeNode);
            returnStr.append(tempStr);
        }
        return returnStr;
    } else {
        if (domNode.parentNode().nodeName() != excludeNode) {
            returnStr = domNode.toText().data();
        }
        return returnStr;
    }
}

///////////////////////////////////////////////////////////////////////////////
//
QString WiktionaryParser::createInsertQuery(WiktionaryEntry we, QString databaseTable)
{
    // TODO: Implement all we fields
    QString query, fields, values;
    WiktionaryParserUtilities wpu;
    QChar singleQuote = '\'';
    QChar delimiter = '#';

    fields.append("(");
    fields.append("word, wordForm, wordUrl, partOfSpeechTag, hyphenations, pronunciations, meanings, etymologies, synonyms, antonyms, hyperonyms, hyponyms, examples");
    fields.append(")");

    values.append("VALUES (");
    // the replace methods secures that all singles quotes are escaped
    values.append(wpu.quoteQString(we.word().replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(we.wordForm().replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(we.wordUrl().replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(we.partOfSpeechTag().replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.hyphenations(), delimiter).replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.pronunciations(), delimiter).replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.meanings(), delimiter).replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.etymologies(), delimiter).replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.synonyms(), delimiter).replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.antonyms(), delimiter).replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.hyperonyms(), delimiter).replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.hyponyms(), delimiter).replace("'", "\\'"), singleQuote) + ", ");
    values.append(wpu.quoteQString(wpu.concatenateQStringList(we.examples(), delimiter).replace("'", "\\'"), singleQuote));
    values.append(")");

    query.append("INSERT INTO " + databaseTable);
    query.append(" " + fields);
    query.append(" " + values);

    return query;
}

///////////////////////////////////////////////////////////////////////////////
// Create the SQL insert query to insert the inflection data to an SQL database
QString WiktionaryParser::createPosTagInsertQuery(WiktionaryEntry we, QString databasetable)
{
    QString query, fields, values;

    QMapIterator<QString, QString> it(we.inflectionData());

    fields.append("(word, ");
    values.append("VALUES ('" + we.word() + "', ");
    while (it.hasNext()) {
         it.next();
         QString key = it.key();
         key.append(", ");
         QString value = it.value();
         // the replace method secures that all singles quotes are escaped
         value.replace("'", "\\'").append("', ").prepend("'");
//         qDebug() << key << value;
        fields.append(key);
        values.append(value);
     }
    fields.remove(fields.length()-2, 2);
    values.remove(values.length()-2, 2);
    fields.append(")");
    values.append(")");

    query.append("INSERT INTO " + databasetable);
    query.append(" " + fields);
    query.append(" " + values);

    return query;
}

// Create the SQL insert query to insert the inflection data (wordforms) to the fullform SQL database
QStringList WiktionaryParser::createFullformInsertQueries(WiktionaryEntry we, QString posTag)
{
    QMap<QString, QString> inflectionData = we.inflectionData();
    QStringList queries, words;
    QString query, wordForm;

    // Query for normal wordform
    query.clear();
    query.append("INSERT INTO fullform (wordForm, word) VALUES ('");
    query.append(we.wordForm());
    query.append("', '");
    query.append(we.word());
    query.append("')");
    queries.append(query);

    if (!inflectionData.isEmpty()) {
        QMap<QString, QString>::iterator i = inflectionData.begin();
        while (i != inflectionData.end()) {
            wordForm = i.value();

            if (i.value() == QString::fromUtf8("—")) {
                i++;
                continue;
            }

            // Check and split multiline entries
            if (wordForm.contains('\n')) {
                 QStringList words = wordForm.split('\n');
                 for (int j = 1; j < words.length(); j++) {
                     inflectionData.insert(i.key() + "-" + j+1, words.at(j));
                 }
                 wordForm = words.at(0);
            }

            // Remove some characters (e.g. !, /, (, ))
            // TODO: Remove further punctuation
            wordForm.remove('!');
            wordForm.remove('/');
            wordForm.remove('(');
            wordForm.remove(')');

            words = wordForm.split(" ");

            // Handle nouns (delete articles)
            if (posTag == "noun") {
                if (words.length() == 2 && words.at(0).startsWith('d')) {
                    wordForm = words.at(1);
                }
            }
            // Handle adjectives (delete "am" in superlative)
            else if (posTag == "adjective") {
                if (i.key() == "Superlative" && words.length() > 1 && words.at(0).startsWith('a')) {
                    wordForm = words.at(1);
                }
            }
            // Handle verbs (ignore auxiliary verb)
            else if (posTag == "verb") {
                if (i.key() == "Auxiliary") {
                    i++;
                    continue;
                }
                else if (words.length() > 1 && words.at(1).startsWith('[')) {
                    wordForm = words.at(0);
                }
            }

            // TODO: Remove "—" entries/entry ;-)

            query.clear();
            query.append("INSERT INTO fullform (wordForm, word) VALUES ('");
            query.append(wordForm);
            query.append("', '");
            query.append(we.word());
            query.append("')");
            queries.append(query);
            i++;
        }
    }

    return queries;
}

// Create the SQL insert query to insert the translation data to an SQL database
QString WiktionaryParser::createTranslationInsertQuery(WiktionaryEntry we, QString databasetable)
{
    QString query, tempTranslation, translations;

    for (int i = 0; i < we.translations().length(); i++) {
        tempTranslation = we.translations().at(i);
        tempTranslation.remove("'");
        translations.append(tempTranslation);
        translations.append("#");
    }

    translations.remove(translations.length()-2, 2);

    query.append("INSERT INTO ");
    query.append(databasetable);
    query.append(" (word, translations) VALUES ('");
    query.append(we.word());
    query.append("', '");
    query.append(translations);
    query.append("')");

    return query;
}
