/*  This file is part of the WikiParser
    Copyright (C) 2011-2012 Mario Fux <fux@kde.org>

    WikiParser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WikiParser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WikiParser.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file
  * This file is part of the WikiParser project and defines the main class
  * of the German wiktionary parser.
  *
  * @author Mario Fux <fux@kde.org>
  */

#ifndef WIKTIONARYPARSER_H
#define WIKTIONARYPARSER_H

#include "wiktionaryentry.h"
#include "wiktionaryparserutilities.h"

#include <QObject>
#include <QtCore/QMap>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtSql/QSqlDatabase>
#include <QtXml/QDomNode>

/**
  * This class provides the main methods for the German wiktionary parser.
  * For the moment this parser methods work just for the German wiktionary entries.
  * The class consists of some public methods for the actual parsing.
  * It uses the WiktionaryEntry class for the data exchange and the methods
  * of the WiktionaryParserUtilities class for convenience methods.
  *
  * @author Mario Fux <fux@kde.org>
  * @since  0.1
  */
class WiktionaryParser : public QObject
{
    Q_OBJECT

///////////////////////////////////////////////////////////////////////////////
// Public methods
public:
    /**
      * This method is the constructor of the WiktionaryParser class
      *
      * @param parent is the parent of the QObject hierarchy (default is 0)
      *
      */
    explicit WiktionaryParser(QObject *parent = 0);

    /**
      * This method is the destructor of the WiktionaryParser class
      *
      */
    ~WiktionaryParser();

    /**
      * This convenience method sets the wiktionary URL.
      *
      * @param wikiUrl is the wiki URL to set
      *
      */
    void setWikiUrl(QString wikiUrl);

    /**
      * This method downloads the list of words of a given part of speech tag,
      * sets it internally and returns it too.
      *
      * @param l10nPosTag is the part of speech tag for which the word list shall be downloaded.
      *
      * @return @c is the downloaded word list as a string list
      *
      */
    QStringList downLoadWordListByPos(QString l10nPosTag);

    /**
      * This method downloads wiktionary pages of a certain part of speech
      * and saves them locally.
      *
      * @param wordList is the list of words for which the pages shall be downloaded
      * @param baseSaveDir is the directory the downloaded pages shall be saved
      * @param posTag is the part of speech tag of which the word list consists
      *
      */
    void downLoadWiktionaryPages(QStringList wordList, QString baseSaveDir, QString posTag);

    // Parses a wiktionary page and extracts all the information
    /**
      * This method parses the wiktionary page extract all the linguistic information.
      * At the moment this works just for German wiktionary pages.
      *
      * @param word is the word string for which the wiktionary pages shall be parsed
      * @param remote is a boolean that states if their is a remote wiktionary or a local file to parse
      *
      * @return @c is a list of WiktionaryEntry entries extracted from the page
      *
      */
    QList<WiktionaryEntry> parseWiktionaryPage(QString word, bool remote = true);

    // Parses a list of words and extracts their information
    /**
      * This method parses the pages of a list of words consisting of a certain
      * part of speech tag.
      *
      * @param posTag is the part of speech tag the list of words to be parsed consists of
      *
      */
    void parseAllWords(QString posTag);

    // Gets a list of words from a local file
    /**
      * This method gets a list of words from a file.
      *
      * @param path is the path to the list file
      * @param posTag is the part of speech tag of the list
      *
      * @return @c is the list of words of a certain part of speech tag
      *
      */
    QStringList getWordListFromFile(QString path, QString posTag);

    bool analyseText(QStringList tokenizedText);

    QMap<QString, QString> getL10nPosTagMap();


///////////////////////////////////////////////////////////////////////////////
// Private variables
private:
    long m_wordCount;
    long m_doubleCount;
    QString m_baseUrl;
    QString m_language;
    QString m_wikiUrl;
    QSqlDatabase m_db;
    QMap<QString, QStringList> m_wordLists;
    QMap<QString, QString> m_posMap;
    QNetworkAccessManager *m_networkManager;
    QStringList m_punctuations;
    QStringList m_endPunctuations;
    QStringList m_startPunctuations;
    QStringList m_articles;

///////////////////////////////////////////////////////////////////////////////
// Private methods

    ///////////////////////////////////////////////////////////////////////////
    // Downloads a (x)html page via HTTP and returns the content
    QByteArray downloadPage(QString urlString);

    ///////////////////////////////////////////////////////////////////////////
    // Methods to get the word list
    ///////////////////////////////
    QStringList getWordList(QString l10nPos);
    int getNumberOfPages(QString urlString);
    QString getNextUrl(QString urlString);
    QStringList parseWordListPage(QByteArray page);
    QStringList getRandomWordList(QStringList wordList, int numberOfWords);

    QString concatenateQStringList(QStringList stringList, QString delimiter);

    ///////////////////////////////////////////////////////////////////////////
    // Methods to parse the linguist information
    ////////////////////////////////////////////
    // Parses the pronunciation information
    QStringList parsePronunciation(QString pronunciationLines);
    // Parses a list of information with bracketed numbers
    QStringList parseNumberedList(QString numberedListLines);
    // Parses the part of speech tag
    QString parsePosTag(QString posTagLine);
    // Parses the inflection table
    QMap<QString, QString> parseInflectionTable(QString posTag, QDomNode tableNode);
    // Parses the declension table of nouns
    QMap<QString, QString> parseDeclensionTable(QDomNode tableNode);
    // Parses the conjugation table of verbs
    QMap<QString, QString> parseConjugationTable(QDomNode tableNode);
    // Parses the comparison table of adjectives
    QMap<QString, QString> parseComparisonTable(QDomNode tableNode);
    // Gets data from a certain cell of the table
    QString getCellData(QDomNode tableNode, unsigned int row, unsigned int column);
    // Converts the dd html tags to a string list
    QStringList parseDdTags(QDomNodeList ddList);
    // Check childNode attributes if they contain a certain string
    bool checkChildNodeAttributes(QDomNodeList childNodes, QString string);
    // Return strings of all (or certain) childdomnodes
    QString getChildNodeStrings(QDomNode domNode, QString excludeNode="");

    ///////////////////////////////////////////////////////////////////////////
    // Database methods
    ///////////////////
    // Create the SQL insert query to insert the parsed information to an SQL database
    QString createInsertQuery(WiktionaryEntry we, QString databaseTable);
    // Create the SQL insert query to insert the inflection data to an SQL database
    QString createPosTagInsertQuery(WiktionaryEntry we, QString databasetable);
    // Create the SQL insert query to insert the inflection data (wordforms) to the fullform SQL database
    QStringList createFullformInsertQueries(WiktionaryEntry we, QString posTag);
    // Create the SQL insert query to insert the translation data to an SQL database
    QString createTranslationInsertQuery(WiktionaryEntry we, QString databasetable);


///////////////////////////////////////////////////////////////////////////////
// Signals
signals:

///////////////////////////////////////////////////////////////////////////////
// Public slots
public slots:

};

#endif // WIKTIONARYPARSER_H
