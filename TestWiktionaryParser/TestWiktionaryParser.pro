# -------------------------------------------------
# Project created by QtCreator 2012-01-14T11:42:31
# -------------------------------------------------
QT += testlib xml sql network
QT -= gui
TARGET = TestWiktionaryParser
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += testwiktionaryparser.cpp \
    ../wiktionaryparser.cpp \
    ../wiktionaryentry.cpp \
    ../wiktionaryparserutilities.cpp
HEADERS += ../wiktionaryparser.h \
    ../wiktionaryentry.h \
    ../wiktionaryparserutilities.h
