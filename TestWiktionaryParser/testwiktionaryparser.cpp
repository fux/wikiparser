#include <QtTest/QtTest>

#include "../wiktionaryentry.h"
#include "../wiktionaryparser.h"
#include "../wiktionaryparserutilities.h"


class TestWiktionaryParser: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void testAdjective();
    void testNoun();
    void testVerb();

private:
    QList<WiktionaryEntry> m_adjectiveList;
    QList<WiktionaryEntry> m_nounList;
    QList<WiktionaryEntry> m_verbList;
};

void TestWiktionaryParser::initTestCase()
{
    WiktionaryParser wp;
    wp.setWikiUrl("/home/fmario/deWiktionary/");
    m_adjectiveList = wp.parseWiktionaryPage("al dente", false);
    m_nounList = wp.parseWiktionaryPage("Kiefer", false);
    m_verbList = wp.parseWiktionaryPage(QString::fromUtf8("überziehen"), false);
}

void TestWiktionaryParser::testAdjective()
{
    for (int i = 0; i < m_adjectiveList.length(); i++)
    {
//        qDebug() << "Bla" << m_adjectiveList.at(i).pronunciations();
    }

    QString str = "Hello";
    QString synonym1 = m_adjectiveList.at(0).synonyms().at(0);
    QString pronunciation1 = m_adjectiveList.at(0).pronunciations().at(0);
    QString etymology1 = m_adjectiveList.at(0).etymologies().at(0);
    QCOMPARE(str.toUpper(), QString("HELLO"));
    QCOMPARE(synonym1, QString("[1] bissfest"));
    QCOMPARE(pronunciation1, QString::fromUtf8("alˈdɛntə"));
//    QCOMPARE(etymology1, QString::fromUtf8("aus dem Italienischen; al dente → it heißt wörtlich „für den Zahn (spürbar)“ – italienisch dente → it ist der der Zahn[1]"));
}

void TestWiktionaryParser::testNoun()
{
    QString str = "Hello";
    QCOMPARE(str.toUpper(), QString("HELLO"));
}


void TestWiktionaryParser::testVerb()
{
    QString str = "Hello";
    QCOMPARE(str.toUpper(), QString("HELLO"));
}


QTEST_MAIN(TestWiktionaryParser)
#include "testwiktionaryparser.moc"
