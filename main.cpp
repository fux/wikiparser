/*  This file is part of the WikiParser
    Copyright (C) 2011-2012 Mario Fux <fux@kde.org>

    WikiParser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WikiParser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WikiParser.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QUrl>

#include "wiktionaryentry.h"
#include "wiktionaryparser.h"
#include "wiktionaryparserutilities.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if (a.arguments().length() < 2 || a.arguments().at(1) == "--help" || a.arguments().at(1) == "-h") {
        qDebug();
        qDebug() << "Usage of the wikiparser (parses german wiktionaries):";
        qDebug() << a.arguments().at(0) << "--help|-h                              -- shows this help text";
        qDebug() << a.arguments().at(0) << "--remote|-r                            -- parses a remote wiktionary";
        qDebug() << a.arguments().at(0) << "--download|-d <destination-directory>/ -- downloads wiktionary pages and word lists";
        qDebug() << a.arguments().at(0) << "--local|-l    <destination-directory>/ -- parses a locally downloaded wiktionary";
        qDebug() << a.arguments().at(0) << "--text|-t     <text-file>              -- analyse text file";
        qDebug() << "The arguments -r and -l expect a local MySQL database named \"wiktionaryParserDb\"";
        qDebug() << "with a user \"wpuser\" and password \"wp123\".";
        qDebug();
        return 1;
    }

    WiktionaryParser wp;
    WiktionaryParserUtilities wu;

    QMap<QString, QString> posMap = wp.getL10nPosTagMap();
    QMap<QString, QString>::const_iterator i;

    if (a.arguments().at(1) == "--remote" || a.arguments().at(1) == "-r") {
        wp.downLoadWordListByPos("adjective");
        wp.downLoadWordListByPos("noun");
        wp.downLoadWordListByPos("verb");
        wp.parseAllWords("adjective");
        wp.parseAllWords("noun");
        wp.parseAllWords("verb");
    }
    else if (a.arguments().length() > 2 && (a.arguments().at(1) == "--download" || a.arguments().at(1) == "-d")) {

        QStringList wordList;
        i = posMap.constBegin();
        while (i != posMap.constEnd()) {
//            wp.downLoadWiktionaryPages(wp.downLoadWordListByPos(i.key()), a.arguments().at(2), i.value());
            wordList = wp.downLoadWordListByPos(i.key());
            wp.downLoadWiktionaryPages(wordList, a.arguments().at(2), i.value());
            i++;
        }
        qDebug() << "Now setup apache and link it so that the locale xhtml files are available under the following URL:";
        qDebug() << "http://localhost/deWiktionary/";
    }
    else if (a.arguments().length() > 2 && (a.arguments().at(1) == "--local" || a.arguments().at(1) == "-l")) {
        // TODO: Change wikiUrl setter to something better and external
        wp.setWikiUrl("http://localhost/deWiktionary/");
        wp.getWordListFromFile(a.arguments().at(2), "test");

        wp.parseAllWords("test");
    }
    else if (a.arguments().length() > 2 && (a.arguments().at(1) == "--text" || a.arguments().at(1) == "-t")) {
        qDebug() << "Text file modus";
        QStringList list = wu.tokenizeText(a.arguments().at(2));
        wp.analyseText(list);

    }
    else if (a.arguments().at(1) == "test") {
//        wp.setWikiUrl("http://localhost/deWiktionary/");
        wp.getWordListFromFile("/home/fmario/deWiktionary/", "all");
        wp.parseAllWords("all");
        // Downloads all wiktionary pages
//        wp.downLoadWiktionaryPages(wp.getWordListFromFile(a.arguments().at(2), "all"), a.arguments().at(2), "all");
    }

    qDebug();
    qDebug() << "End of program. Press Ctrl+C to quit.";

    return a.exec();
}
