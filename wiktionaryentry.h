/*  This file is part of the WikiParser
    Copyright (C) 2011-2012 Mario Fux <fux@kde.org>

    WikiParser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WikiParser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WikiParser.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file
  * This file is part of the WikiParser project and defines the content
  * and some methods for a Wiktionary entry.
  *
  * @author Mario Fux <fux@kde.org>
  */

#ifndef WIKTIONARYENTRY_H
#define WIKTIONARYENTRY_H

#include <QtCore/QMap>
#include <QtCore/QStringList>

/**
  * This class represents the data of a wiktionary entry.
  * For the moment this class can represent just German wiktionary entries.
  * The class consists of some private data members, some getter and setters methods
  * and some few convenience methods.
  *
  * @author Mario Fux <fux@kde.org>
  * @since  0.1
  */
class WiktionaryEntry
{
///////////////////////////////////////////////////////////////////////////////
// Public methods
public:

    /**
      * This method is the constructor of the WiktionaryEntry class
      *
      * @param word to assign to the private m_word string of the wiktionary entry
      *        (see also wordForm for the word form of the entry)
      *
      */
    WiktionaryEntry(QString word);

    /**
      * This method clears the content of the WiktionaryEntry
      */
    void clear();

    /**
      * This method prints the class content via qdebug
      * The method can be used for debug purposes.
      */
    void showContent();

    ///////////////////////////////////////////////////////////////////////////
    // Setter methods
    /**
      * This method sets the word of the wiktionary entry
      *
      * @param word to assign to the private m_word string of the wiktionary entry
      *        (see also wordForm for the word form of the entry)
      *
      */
    void setWord(const QString word);

    /**
      * This method sets the word form of the wiktionary entry
      *
      * @param wordForm to assign to the private m_wordForm string of the wiktionary entry
      *
      */
    void setWordForm(const QString wordForm);

    /**
      * This method sets the word URL of the wiktionary entry
      *
      * @param wordUrl to assign to the private m_wordUrl string of the wiktionary entry
      *
      */
    void setWordUrl(const QString wordUrl);

    /**
      * This method sets the part of speech tag of the word of the wiktionary entry
      *
      * @param partOfSpeechTag to assign to the private m_partOfSpeechtag string of the wiktionary entry
      *
      */
    void setPartOfSpeechTag(const QString partOfSpeechTag);

    /**
      * This method sets the hyphenations list of the wiktionary entry
      *
      * @param hyphenations to assign to the private m_hyphenations string list of the wiktionary entry
      *
      */
    void setHyphenations(const QStringList hyphenations);

    /**
      * This method sets the pronunciations list of the wiktionary entry
      *
      * @param pronunciations to assign to the private m_pronuciations string list of the wiktionary entry
      *
      */
    void setPronunciations(const QStringList pronunciations);

    /**
      * This method sets the meanings list of the wiktionary entry
      *
      * @param meanings to assign to the m_meanings string list of the wiktionary entry
      *
      */
    void setMeanings(const QStringList meanings);

    /**
      * This method sets the etymologies list of the wiktionary entry
      *
      * @param etymologies to assign to the m_etymologies string list of the wiktionary entry
      *
      */
    void setEtymologies(const QStringList etymologies);

    /**
      * This method sets the synonyms list of the wiktionary entry
      *
      * @param synonyms to assign to the m_synonyms string list of the wiktionary entry
      *
      */
    void setSynonyms(const QStringList synonyms);

    /**
      * This method sets the antonyms list of the wiktionary entry
      *
      * @param antonyms to assign to the m_antonyms string list of the wiktionary entry
      *
      */
    void setAntonyms(const QStringList antonyms);

    /**
      * This method sets the hyperonyms list of the wiktionary entry
      *
      * @param hyperonyms to assign to the m_hyperonyms string list of the wiktionary entry
      *
      */
    void setHyperonyms(const QStringList hyperonyms);

    /**
      * This method sets the hyponyms list of the wiktionary entry
      *
      * @param hyponyms to assign to the m_hyponyms string list of the wiktionary entry
      *
      */
    void setHyponyms(const QStringList hyponyms);

    /**
      * This method sets the examples list of the wiktionary entry
      *
      * @param examples to assign to the m_examples string list of the wiktionary entry
      *
      */
    void setExamples(const QStringList examples);

    /**
      * This method sets the translations list of the wiktionary entry
      *
      * @param examples to assign to the m_translations string list of the wiktionary entry
      *
      */
    void setTranslations(const QStringList translations);

    /**
      * This method sets the inflection data of the wiktionary entry
      *
      * @param inflectionData to assign to the m_inflectionData map of the wiktionary entry
      *        the map contains a key string (grammar categorie) and a value string (grammar value)
      *
      */
    void setInflectionData(const QMap<QString, QString> inflectionData);


    ///////////////////////////////////////////////////////////////////////////
    // Getter methods
    /**
      * This method returns the word string of the wiktionary entry
      *
      * @return @c word as string
      *
      */
    QString word() const;

    /**
      * This method returns the word form string of the wiktionary entry
      *
      * @return @c word form as string
      *
      */
    QString wordForm() const;

    /**
      * This method returns the word url string of the wiktionary entry
      *
      * @return @c word url as string
      *
      */
    QString wordUrl() const;

    /**
      * This method returns the part of speech tag of the word of the wiktionary entry
      *
      * @return @c part of speech tag as string
      *
      */
    QString partOfSpeechTag() const;

    /**
      * This method returns the hyphenations list of the wiktionary entry
      *
      * @return @c hyphenations list as string list
      *
      */
    QStringList hyphenations() const;

    /**
      * This method returns the pronunciations list of the wiktionary entry
      *
      * @return @c pronunciations list as string list
      *
      */
    QStringList pronunciations() const;

    /**
      * This method returns the meanings list of the wiktionary entry
      *
      * @return @c meanings list as string list
      *
      */
    QStringList meanings() const;

    /**
      * This method returns the etymologies list of the wiktionary entry
      *
      * @return @c etymologies list as string list
      *
      */
    QStringList etymologies() const;

    /**
      * This method returns the synonyms list of the wiktionary entry
      *
      * @return @c synonyms list as string list
      *
      */
    QStringList synonyms() const;

    /**
      * This method returns the antonyms list of the wiktionary entry
      *
      * @return @c antonyms list as string list
      *
      */
    QStringList antonyms() const;

    /**
      * This method returns the hyperonyms list of the wiktionary entry
      *
      * @return @c hyperonyms list as string list
      *
      */
    QStringList hyperonyms() const;

    /**
      * This method returns the hyponyms list of the wiktionary entry
      *
      * @return @c hyponyms list as string list
      *
      */
    QStringList hyponyms() const;

    /**
      * This method returns the examples list of the wiktionary entry
      *
      * @return @c examples list as string list
      *
      */
    QStringList examples() const;

    /**
      * This method returns the translations list of the wiktionary entry
      *
      * @return @c translations list as string list
      *
      */
    QStringList translations() const;

    /**
      * This method returns the inflection data of the wiktionary entry
      *
      * @return @c inflection data as map of string keys and string values
      *
      */
    QMap<QString, QString> inflectionData() const;

///////////////////////////////////////////////////////////////////////////////
// Private variables
private:
    QString m_word;
    QString m_wordForm;
    QString m_wordUrl;
    QString m_partOfSpeechTag;
    QStringList m_hyphenations;
    QStringList m_pronunciations;
    QStringList m_meanings;
    QStringList m_etymologies;
    QStringList m_synonyms;
    QStringList m_antonyms;
    QStringList m_hyperonyms;
    QStringList m_hyponyms;
    QStringList m_examples;
    QStringList m_translations;
    QMap<QString, QString> m_inflectionData;
};

#endif // WIKTIONARYENTRY_H
