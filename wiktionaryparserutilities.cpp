/*  This file is part of the WikiParser
    Copyright (C) 2011-2012 Mario Fux <fux@kde.org>

    WikiParser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WikiParser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WikiParser.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>

#include "wiktionaryparserutilities.h"

///////////////////////////////////////////////////////////////////////////////
// Constructors and destructors
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Constructor
WiktionaryParserUtilities::WiktionaryParserUtilities()
{
}

///////////////////////////////////////////////////////////////////////////////
// Public methods
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Save a word list to a file
void WiktionaryParserUtilities::saveWordList(QStringList wordList, QString filename)
{
    QByteArray saveContent;

    QFile savefile(filename);

    if (!savefile.open(QIODevice::Append)) {
        qDebug() << "Error saving wordList file...";
    }

    QString str;
    foreach (str, wordList) {
        saveContent.append(str.toUtf8()).append('\n');
    }

    savefile.write(saveContent);
    savefile.close();
}

///////////////////////////////////////////////////////////////////////////////
// Save a page to a file
void WiktionaryParserUtilities::savePage(QByteArray content, QString filename)
{
    QFile savefile(filename);

    if (!savefile.open(QIODevice::WriteOnly)) {
        qDebug() << "Error saving page...";
    }

    savefile.write(content);
    savefile.close();
}

///////////////////////////////////////////////////////////////////////////////
//
QString WiktionaryParserUtilities::concatenateQStringList(QStringList stringList, QChar delimiter)
{
    QString result;

    QString tempStr;
    foreach (tempStr, stringList) {
        result.append(tempStr).append(delimiter);
    }

    result.remove(result.length()-1, 1);

    return result;
}

///////////////////////////////////////////////////////////////////////////////
//
QString WiktionaryParserUtilities::quoteQString(QString string, QChar quote)
{
    return string.prepend(quote).append(quote);
}


///////////////////////////////////////////////////////////////////////////////
// Translates the german pos tag to english
QString WiktionaryParserUtilities::getL10nPosTag(QString posTag)
{
    QString l10nPosTag;

    // TODO: Scheint nicht darauf zu achten, d.h. Strings, die so anfangen, werden scheinbar doch nicht gematched

    if (posTag.startsWith("Verb")) {
        l10nPosTag = "verb";
    }
    else if (posTag.startsWith("Substantiv") || posTag.startsWith("Nomen")) {
        l10nPosTag = "noun";
    }
    else if (posTag.startsWith("Adjektiv")) {
        l10nPosTag = "adjective";
    }

    return l10nPosTag;
}

///////////////////////////////////////////////////////////////////////////////
// Translates the german pos tags to english ones
QStringList WiktionaryParserUtilities::getL10nPosTags(QStringList posTags)
{
    QStringList l10nPosTags;

    if (posTags.at(0).startsWith("Verb")) {
        l10nPosTags.append("verb");
    }
    else if (posTags.at(0).startsWith("Substantiv") || posTags.at(0).startsWith("Nomen")) {
        l10nPosTags.append("noun");
    }
    else if (posTags.at(0).startsWith("Adjektiv")) {
        l10nPosTags.append("adjective");
    }

    return l10nPosTags;
}

QStringList WiktionaryParserUtilities::tokenizeText(QString filename)
{
    QFile readfile(filename);
    if (!readfile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error reading file...";
    }

    QString content(QString::fromUtf8(readfile.readAll()));

    QStringList tempList = content.simplified().split(" ");
    QStringList list;

    QString word;
    QStringList::const_iterator constIterator;
    for (constIterator = tempList.constBegin(); constIterator != tempList.constEnd(); ++constIterator) {
        word = (*constIterator).trimmed();

        // TODO: Make this punctuation detection and removal better
        if (word.endsWith(".")) {
            word = word.replace(".", "");
        }
        else if (word.endsWith(",")) {
            word = word.replace(",", "");
        }
        else if (word.endsWith(":")) {
            word = word.replace(":", "");
        }
        else if (word.endsWith(";")) {
            word = word.replace(";", "");
        }
        else if (word.endsWith("?")) {
            word = word.replace("?", "");
        }
        else if (word.endsWith("!")) {
            word = word.replace("!", "");
        }

        word.remove(QString::fromUtf8("«"));
        word.remove(QString::fromUtf8("»"));
        word.remove("(");
        word.remove(")");
        word.remove("\"");

        list.append(word);
    }


//    qDebug() << list;

    readfile.close();

    return list;
}


