/*  This file is part of the WikiParser
    Copyright (C) 2011-2012 Mario Fux <fux@kde.org>

    WikiParser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WikiParser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WikiParser.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file
  * This file is part of the WikiParser project and defines a class
  * with some convenient helper methods for the wiktionary parser.
  *
  * @author Mario Fux <fux@kde.org>
  */

#ifndef WIKTIONARYPARSERUTILITIES_H
#define WIKTIONARYPARSERUTILITIES_H

#include <QtCore/QByteArray>
#include <QtCore/QChar>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtSql/QSqlDatabase>


/**
  * This class provides some convenience methods for the Wiktionary parser.
  * These methods provide e.g. some special string handling and preparations
  * or the saving of words lists to files.
  *
  * @author Mario Fux <fux@kde.org>
  * @since  0.1
  */
class WiktionaryParserUtilities
{
public:
    /**
      * This method is the constructor of the WiktionaryParserUtilities class
      * It doesn't take any parameter.
      */
    WiktionaryParserUtilities();

    ///////////////////////////////////////////////////////////////////////////
    // Storage methods
    /**
      * This method saves a word list to a file.
      *
      * @param wordList is the list of words to save
      * @param filename is the name of the file used to save the word list
      *
      */
    void saveWordList(QStringList wordList, QString filename);

    /**
      * This method saves the content of a web page to a file.
      *
      * @param content is the content (as QByteArray) of the web page to be saved
      * @param filename is the name of the file used to save the web page content
      *
      */
    void savePage(QByteArray content, QString filename);

    ///////////////////////////////////////////////////////////////////////////
    // String methods
    /**
      * This method concatenates a list of strings to a string devided separated by a delimiter.
      *
      * @param stringList is the list of strings to be concatenated
      * @param delimiter is the delimiter to be used for the string separation
      *
      * @return @c is the concatenated string
      *
      */
    QString concatenateQStringList(QStringList stringList, QChar delimiter);

    /**
      * This method appends and prepends a string with some kind of quotes (or other symbols).
      *
      * @param string is the to be quoted string
      * @param quote is the character (symbol) used to quote the string
      *
      * @return @c is the quoted string
      *
      */
    QString quoteQString(QString string, QChar quote);

    ///////////////////////////////////////////////////////////////////////////
    // Localize part of speech tags
    /**
      * This method takes a localized (atm German) part of speech tag and returns
      * it in the english form (e.g. "Substantiv" -> "noun").
      *
      * @param posTag is the localized part of speech tag as string
      *
      * @return @c is the english form of the part of speech tag as string
      *
      */
    QString getL10nPosTag(QString posTag);

    /**
      * This method takes a list of localized (atm German) part of speech tags and returns
      * it in the english forms (e.g. "Substantiv" -> "noun").
      *
      * @param posTags is the list of localized part of speech tags
      *
      * @return @c is the list of english forms of the part of speech tags
      *
      */
    QStringList getL10nPosTags(QStringList posTags);

    QStringList tokenizeText(QString filename);
private:
    QSqlDatabase m_db;
};

#endif // WIKTIONARYPARSERUTILITIES_H
