/*  This file is part of the WikiParser
    Copyright (C) 2011-2012 Mario Fux <fux@kde.org>

    WikiParser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WikiParser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WikiParser.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QDebug>

#include "wiktionaryentry.h"

///////////////////////////////////////////////////////////////////////////////
// Constructors and destructors
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Constructor
WiktionaryEntry::WiktionaryEntry(QString word)
{
    m_word = word;
}



///////////////////////////////////////////////////////////////////////////////
// Public methods
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Clears the content of this WiktionaryEntry
void WiktionaryEntry::clear()
{
    m_word.clear();
    m_wordForm.clear();
    m_wordUrl.clear();
    m_partOfSpeechTag.clear();
    m_hyphenations.clear();
    m_pronunciations.clear();
    m_meanings.clear();
    m_etymologies.clear();
    m_synonyms.clear();
    m_antonyms.clear();
    m_hyperonyms.clear();
    m_hyponyms.clear();
    m_examples.clear();
    m_translations.clear();
    m_inflectionData.clear();
}

///////////////////////////////////////////////////////////////////////////////
// Print content via qdebug
void WiktionaryEntry::showContent()
{
    qDebug();
    qDebug() << "Wiktionary Entry for:" << m_word;
    qDebug() << "WordForm:      " << m_wordForm;
    qDebug() << "WordUrl:       " << m_wordUrl;
    qDebug() << "PoS tag:       " << m_partOfSpeechTag;
    qDebug() << "Hypenations:   " << m_hyphenations;
    qDebug() << "Pronunciations:" << m_pronunciations;
    qDebug() << "Meanings:      " << m_meanings;
    qDebug() << "Etymologies:   " << m_etymologies;
    qDebug() << "Synonyms:      " << m_synonyms;
    qDebug() << "Antonyms:      " << m_antonyms;
    qDebug() << "Hyperonyms:    " << m_hyperonyms;
    qDebug() << "Hyponyms:      " << m_hyponyms;
    qDebug() << "Examples:      " << m_examples;
    qDebug() << "Translations:  " << m_translations;
    qDebug() << "InflectionData;" << m_inflectionData;
    qDebug();
}

///////////////////////////////////////////////////////////////////////////////
// Setter methods
void WiktionaryEntry::setWord(const QString word)
{
    m_word = word;
}

void WiktionaryEntry::setWordForm(const QString wordForm)
{
    m_wordForm = wordForm;
}

void WiktionaryEntry::setWordUrl(QString wordUrl)
{
    m_wordUrl = wordUrl;
}

void WiktionaryEntry::setPartOfSpeechTag(const QString partOfSpeechTag)
{
    m_partOfSpeechTag = partOfSpeechTag;
}

void WiktionaryEntry::setHyphenations(const QStringList hyphenations)
{
    m_hyphenations = hyphenations;
}

void WiktionaryEntry::setPronunciations(const QStringList pronunciations)
{
    m_pronunciations = pronunciations;
}

void WiktionaryEntry::setMeanings(const QStringList meanings)
{
    m_meanings = meanings;
}

void WiktionaryEntry::setEtymologies(const QStringList etymologies)
{
    m_etymologies = etymologies;
}

void WiktionaryEntry::setSynonyms(const QStringList synonyms)
{
    m_synonyms = synonyms;
}

void WiktionaryEntry::setAntonyms(const QStringList antonyms)
{
    m_antonyms = antonyms;
}

void WiktionaryEntry::setHyperonyms(const QStringList hyperonyms)
{
    m_hyperonyms = hyperonyms;
}

void WiktionaryEntry::setHyponyms(const QStringList hyponyms)
{
    m_hyponyms = hyponyms;
}

void WiktionaryEntry::setExamples(const QStringList examples)
{
    m_examples = examples;
}

void WiktionaryEntry::setTranslations(const QStringList translations)
{
    m_translations = translations;
}

void WiktionaryEntry::setInflectionData(QMap<QString, QString>inflectionData)
{
    m_inflectionData = inflectionData;
}

///////////////////////////////////////////////////////////////////////////////
// Getter methods
QString WiktionaryEntry::word() const
{
    return m_word;
}

QString WiktionaryEntry::wordForm() const
{
    return m_wordForm;
}

QString WiktionaryEntry::wordUrl() const
{
    return m_wordUrl;
}

QString WiktionaryEntry::partOfSpeechTag() const
{
    return m_partOfSpeechTag;
}

QStringList WiktionaryEntry::hyphenations() const
{
    return m_hyphenations;
}

QStringList WiktionaryEntry::pronunciations() const
{
    return m_pronunciations;
}

QStringList WiktionaryEntry::meanings() const
{
    return m_meanings;
}

QStringList WiktionaryEntry::etymologies() const
{
    return m_etymologies;
}

QStringList WiktionaryEntry::synonyms() const
{
    return m_synonyms;
}

QStringList WiktionaryEntry::antonyms() const
{
    return m_antonyms;
}

QStringList WiktionaryEntry::hyperonyms() const
{
    return m_hyperonyms;
}

QStringList WiktionaryEntry::hyponyms() const
{
    return m_hyponyms;
}

QStringList WiktionaryEntry::examples() const
{
    return m_examples;
}

QStringList WiktionaryEntry::translations() const
{
    return m_translations;
}

QMap<QString, QString> WiktionaryEntry::inflectionData() const
{
    return m_inflectionData;
}
